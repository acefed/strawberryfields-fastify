const crypto = require("crypto");
const fs = require('fs');
const path = require("path");
const fastify = require("fastify");

require("dotenv").config();
let configJson;
if (process.env.CONFIG_JSON) {
  configJson = process.env.CONFIG_JSON;
  if (configJson.startsWith("'")) configJson = configJson.slice(1);
  if (configJson.endsWith("'")) configJson = configJson.slice(0, -1);
} else configJson = fs.readFileSync("data/config.json", 'utf8');
const CONFIG = JSON.parse(configJson);
const ME = [
  '<a href="https://',
  new URL(CONFIG.actor[0].me).hostname,
  '/" rel="me nofollow noopener noreferrer" target="_blank">',
  "https://",
  new URL(CONFIG.actor[0].me).hostname,
  "/",
  "</a>",
].join("");
let privateKeyPem = process.env.PRIVATE_KEY;
privateKeyPem = privateKeyPem.split("\\n").join("\n");
if (privateKeyPem.startsWith('"')) privateKeyPem = privateKeyPem.slice(1);
if (privateKeyPem.endsWith('"')) privateKeyPem = privateKeyPem.slice(0, -1);
const PRIVATE_KEY = privateKeyPem;
const publicKeyPem = crypto.createPublicKey(PRIVATE_KEY).export({ type: "spki", format: "pem" });

const app = fastify({ trustProxy: true });
app.register(require("@fastify/static"), { root: path.join(__dirname, "public")});
app.addContentTypeParser("*", { parseAs: "string" }, app.getDefaultJsonParser());

function uuidv7() {
  const v = new Uint8Array(16);
  crypto.getRandomValues(v);
  const ts = BigInt(Date.now());
  v[0] = Number(ts >> 40n);
  v[1] = Number(ts >> 32n);
  v[2] = Number(ts >> 24n);
  v[3] = Number(ts >> 16n);
  v[4] = Number(ts >> 8n);
  v[5] = Number(ts);
  v[6] = v[6] & 0x0f | 0x70;
  v[8] = v[8] & 0x3f | 0x80;
  return Array.from(v, (b) => b.toString(16).padStart(2, "0")).join("");
}

function talkScript(req) {
  const ts = Date.now();
  if (new URL(req).hostname === "localhost") return `<p>${ts}</p>`;
  return [
    "<p>",
    '<a href="https://',
    new URL(req).hostname,
    '/" rel="nofollow noopener noreferrer" target="_blank">',
    new URL(req).hostname,
    "</a>",
    "</p>",
  ].join("");
}

async function getActivity(username, hostname, req) {
  const t = new Date().toUTCString();
  const sig = crypto
    .createSign("sha256")
    .update(
      [
        `(request-target): get ${new URL(req).pathname}`,
        `host: ${new URL(req).hostname}`,
        `date: ${t}`,
      ].join("\n"),
    )
    .end();
  const b64 = sig.sign(PRIVATE_KEY, "base64");
  const headers = {
    Date: t,
    Signature: [
      `keyId="https://${hostname}/u/${username}#Key"`,
      'algorithm="rsa-sha256"',
      'headers="(request-target) host date"',
      `signature="${b64}"`,
    ].join(),
    Accept: "application/activity+json",
    "Accept-Encoding": "identity",
    "Cache-Control": "no-cache",
    "User-Agent": `StrawberryFields-Fastify/3.0.0 (+https://${hostname}/)`,
  };
  const res = await fetch(req, { method: "GET", headers });
  const status = res.status;
  console.log(`GET ${req} ${status}`);
  const body = await res.json();
  return body;
}

async function postActivity(username, hostname, req, x) {
  const t = new Date().toUTCString();
  const body = JSON.stringify(x);
  const s256 = crypto.createHash("sha256").update(body).digest("base64");
  const sig = crypto
    .createSign("sha256")
    .update(
      [
        `(request-target): post ${new URL(req).pathname}`,
        `host: ${new URL(req).hostname}`,
        `date: ${t}`,
        `digest: SHA-256=${s256}`,
      ].join("\n"),
    )
    .end();
  const b64 = sig.sign(PRIVATE_KEY, "base64");
  const headers = {
    Date: t,
    Digest: `SHA-256=${s256}`,
    Signature: [
      `keyId="https://${hostname}/u/${username}#Key"`,
      'algorithm="rsa-sha256"',
      'headers="(request-target) host date digest"',
      `signature="${b64}"`,
    ].join(),
    Accept: "application/json",
    "Accept-Encoding": "gzip",
    "Cache-Control": "max-age=0",
    "Content-Type": "application/activity+json",
    "User-Agent": `StrawberryFields-Fastify/3.0.0 (+https://${hostname}/)`,
  };
  console.log(`POST ${req} ${body}`);
  await fetch(req, { method: "POST", body, headers });
}

async function acceptFollow(username, hostname, x, y) {
  const aid = uuidv7();
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `https://${hostname}/u/${username}/s/${aid}`,
    type: "Accept",
    actor: `https://${hostname}/u/${username}`,
    object: y,
  };
  await postActivity(username, hostname, x.inbox, body);
}

async function follow(username, hostname, x) {
  const aid = uuidv7();
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `https://${hostname}/u/${username}/s/${aid}`,
    type: "Follow",
    actor: `https://${hostname}/u/${username}`,
    object: x.id,
  };
  await postActivity(username, hostname, x.inbox, body);
}

async function undoFollow(username, hostname, x) {
  const aid = uuidv7();
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `https://${hostname}/u/${username}/s/${aid}#Undo`,
    type: "Undo",
    actor: `https://${hostname}/u/${username}`,
    object: {
      id: `https://${hostname}/u/${username}/s/${aid}`,
      type: "Follow",
      actor: `https://${hostname}/u/${username}`,
      object: x.id,
    },
  };
  await postActivity(username, hostname, x.inbox, body);
}

async function like(username, hostname, x, y) {
  const aid = uuidv7();
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `https://${hostname}/u/${username}/s/${aid}`,
    type: "Like",
    actor: `https://${hostname}/u/${username}`,
    object: x.id,
  };
  await postActivity(username, hostname, y.inbox, body);
}

async function undoLike(username, hostname, x, y) {
  const aid = uuidv7();
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `https://${hostname}/u/${username}/s/${aid}#Undo`,
    type: "Undo",
    actor: `https://${hostname}/u/${username}`,
    object: {
      id: `https://${hostname}/u/${username}/s/${aid}`,
      type: "Like",
      actor: `https://${hostname}/u/${username}`,
      object: x.id,
    },
  };
  await postActivity(username, hostname, y.inbox, body);
}

async function announce(username, hostname, x, y) {
  const aid = uuidv7();
  const t = new Date().toISOString().substring(0, 19) + "Z";
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `https://${hostname}/u/${username}/s/${aid}/activity`,
    type: "Announce",
    actor: `https://${hostname}/u/${username}`,
    published: t,
    to: ["https://www.w3.org/ns/activitystreams#Public"],
    cc: [`https://${hostname}/u/${username}/followers`],
    object: x.id,
  };
  await postActivity(username, hostname, y.inbox, body);
}

async function undoAnnounce(username, hostname, x, y, z) {
  const aid = uuidv7();
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `https://${hostname}/u/${username}/s/${aid}#Undo`,
    type: "Undo",
    actor: `https://${hostname}/u/${username}`,
    object: {
      id: `${z}/activity`,
      type: "Announce",
      actor: `https://${hostname}/u/${username}`,
      to: ["https://www.w3.org/ns/activitystreams#Public"],
      cc: [`https://${hostname}/u/${username}/followers`],
      object: x.id,
    },
  };
  await postActivity(username, hostname, y.inbox, body);
}

async function createNote(username, hostname, x, y) {
  const aid = uuidv7();
  const t = new Date().toISOString().substring(0, 19) + "Z";
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `https://${hostname}/u/${username}/s/${aid}/activity`,
    type: "Create",
    actor: `https://${hostname}/u/${username}`,
    published: t,
    to: ["https://www.w3.org/ns/activitystreams#Public"],
    cc: [`https://${hostname}/u/${username}/followers`],
    object: {
      id: `https://${hostname}/u/${username}/s/${aid}`,
      type: "Note",
      attributedTo: `https://${hostname}/u/${username}`,
      content: talkScript(y),
      url: `https://${hostname}/u/${username}/s/${aid}`,
      published: t,
      to: ["https://www.w3.org/ns/activitystreams#Public"],
      cc: [`https://${hostname}/u/${username}/followers`],
    },
  };
  await postActivity(username, hostname, x.inbox, body);
}

async function createNoteImage(username, hostname, x, y, z) {
  const aid = uuidv7();
  const t = new Date().toISOString().substring(0, 19) + "Z";
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `https://${hostname}/u/${username}/s/${aid}/activity`,
    type: "Create",
    actor: `https://${hostname}/u/${username}`,
    published: t,
    to: ["https://www.w3.org/ns/activitystreams#Public"],
    cc: [`https://${hostname}/u/${username}/followers`],
    object: {
      id: `https://${hostname}/u/${username}/s/${aid}`,
      type: "Note",
      attributedTo: `https://${hostname}/u/${username}`,
      content: talkScript("https://localhost"),
      url: `https://${hostname}/u/${username}/s/${aid}`,
      published: t,
      to: ["https://www.w3.org/ns/activitystreams#Public"],
      cc: [`https://${hostname}/u/${username}/followers`],
      attachment: [
        {
          type: "Image",
          mediaType: z,
          url: y,
        },
      ],
    },
  };
  await postActivity(username, hostname, x.inbox, body);
}

async function createNoteMention(username, hostname, x, y, z) {
  const aid = uuidv7();
  const t = new Date().toISOString().substring(0, 19) + "Z";
  const at = `@${y.preferredUsername}@${new URL(y.inbox).hostname}`;
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `https://${hostname}/u/${username}/s/${aid}/activity`,
    type: "Create",
    actor: `https://${hostname}/u/${username}`,
    published: t,
    to: ["https://www.w3.org/ns/activitystreams#Public"],
    cc: [`https://${hostname}/u/${username}/followers`],
    object: {
      id: `https://${hostname}/u/${username}/s/${aid}`,
      type: "Note",
      attributedTo: `https://${hostname}/u/${username}`,
      inReplyTo: x.id,
      content: talkScript(z),
      url: `https://${hostname}/u/${username}/s/${aid}`,
      published: t,
      to: ["https://www.w3.org/ns/activitystreams#Public"],
      cc: [`https://${hostname}/u/${username}/followers`],
      tag: [
        {
          type: "Mention",
          name: at,
        },
      ],
    },
  };
  await postActivity(username, hostname, y.inbox, body);
}

async function createNoteHashtag(username, hostname, x, y, z) {
  const aid = uuidv7();
  const t = new Date().toISOString().substring(0, 19) + "Z";
  const body = {
    "@context": ["https://www.w3.org/ns/activitystreams", { Hashtag: "as:Hashtag" }],
    id: `https://${hostname}/u/${username}/s/${aid}/activity`,
    type: "Create",
    actor: `https://${hostname}/u/${username}`,
    published: t,
    to: ["https://www.w3.org/ns/activitystreams#Public"],
    cc: [`https://${hostname}/u/${username}/followers`],
    object: {
      id: `https://${hostname}/u/${username}/s/${aid}`,
      type: "Note",
      attributedTo: `https://${hostname}/u/${username}`,
      content: talkScript(y),
      url: `https://${hostname}/u/${username}/s/${aid}`,
      published: t,
      to: ["https://www.w3.org/ns/activitystreams#Public"],
      cc: [`https://${hostname}/u/${username}/followers`],
      tag: [
        {
          type: "Hashtag",
          name: `#${z}`,
        },
      ],
    },
  };
  await postActivity(username, hostname, x.inbox, body);
}

async function updateNote(username, hostname, x, y) {
  const t = new Date().toISOString().substring(0, 19) + "Z";
  const ts = Date.now();
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `${y}#${ts}`,
    type: "Update",
    actor: `https://${hostname}/u/${username}`,
    published: t,
    to: ["https://www.w3.org/ns/activitystreams#Public"],
    cc: [`https://${hostname}/u/${username}/followers`],
    object: {
      id: y,
      type: "Note",
      attributedTo: `https://${hostname}/u/${username}`,
      content: talkScript("https://localhost"),
      url: y,
      updated: t,
      to: ["https://www.w3.org/ns/activitystreams#Public"],
      cc: [`https://${hostname}/u/${username}/followers`],
    },
  };
  await postActivity(username, hostname, x.inbox, body);
}

async function deleteTombstone(username, hostname, x, y) {
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `${y}#Delete`,
    type: "Delete",
    actor: `https://${hostname}/u/${username}`,
    object: {
      id: y,
      type: "Tombstone",
    },
  };
  await postActivity(username, hostname, x.inbox, body);
}

app.get("/", (_req, res) => res.type("text/plain; charset=utf-8").send("StrawberryFields Fastify"));

app.get("/about", (_req, res) => res.type("text/plain; charset=utf-8").send("About: Blank"));

app.get("/u/:username", (req, res) => {
  const username = req.params.username;
  const hostname = new URL(CONFIG.origin).hostname;
  const acceptHeaderField = req.headers["accept"];
  let hasType = false;
  if (username !== CONFIG.actor[0].preferredUsername) return res.callNotFound();
  if (acceptHeaderField?.includes("application/activity+json")) hasType = true;
  if (acceptHeaderField?.includes("application/ld+json")) hasType = true;
  if (acceptHeaderField?.includes("application/json")) hasType = true;
  if (!hasType) {
    const body = `${username}: ${CONFIG.actor[0].name}`;
    const headers = {
      "Cache-Control": `public, max-age=${Number(CONFIG.ttl) || 0}, must-revalidate`,
      Vary: "Accept, Accept-Encoding",
    };
    return res.headers(headers).type("text/plain; charset=utf-8").send(body);
  }
  const body = {
    "@context": [
      "https://www.w3.org/ns/activitystreams",
      "https://w3id.org/security/v1",
      {
        schema: "https://schema.org/",
        PropertyValue: "schema:PropertyValue",
        value: "schema:value",
        Key: "sec:Key",
      },
    ],
    id: `https://${hostname}/u/${username}`,
    type: "Person",
    inbox: `https://${hostname}/u/${username}/inbox`,
    outbox: `https://${hostname}/u/${username}/outbox`,
    following: `https://${hostname}/u/${username}/following`,
    followers: `https://${hostname}/u/${username}/followers`,
    preferredUsername: username,
    name: CONFIG.actor[0].name,
    summary: "<p>3.0.0</p>",
    url: `https://${hostname}/u/${username}`,
    endpoints: { sharedInbox: `https://${hostname}/u/${username}/inbox` },
    attachment: [
      {
        type: "PropertyValue",
        name: "me",
        value: ME,
      },
    ],
    icon: {
      type: "Image",
      mediaType: "image/png",
      url: `https://${hostname}/public/${username}u.png`,
    },
    image: {
      type: "Image",
      mediaType: "image/png",
      url: `https://${hostname}/public/${username}s.png`,
    },
    publicKey: {
      id: `https://${hostname}/u/${username}#Key`,
      type: "Key",
      owner: `https://${hostname}/u/${username}`,
      publicKeyPem,
    },
  };
  const headers = {
    "Cache-Control": `public, max-age=${Number(CONFIG.ttl) || 0}, must-revalidate`,
    Vary: "Accept, Accept-Encoding",
  };
  res.headers(headers).type("application/activity+json").send(body);
});

app.get("/u/:username/inbox", async (_req, res) => res.code(405).send(new Error(res.statusCode)));
app.post("/u/:username/inbox", async (req, res) => {
  const username = req.params.username;
  const hostname = new URL(CONFIG.origin).hostname;
  const contentTypeHeaderField = req.headers["content-type"];
  let hasType = false;
  const y = req.body;
  let t = y.type ?? "";
  const aid = y.id ?? "";
  const atype = y.type ?? "";
  if (aid.length > 1024 || atype.length > 64) return res.code(400).send(new Error(res.statusCode));
  console.log(`INBOX ${aid} ${atype}`);
  if (username !== CONFIG.actor[0].preferredUsername) return res.callNotFound();
  if (contentTypeHeaderField?.includes("application/activity+json")) hasType = true;
  if (contentTypeHeaderField?.includes("application/ld+json")) hasType = true;
  if (contentTypeHeaderField?.includes("application/json")) hasType = true;
  if (!hasType) return res.code(400).send(new Error(res.statusCode));
  if (!req.headers["digest"] || !req.headers["signature"]) return res.code(400).send(new Error(res.statusCode));
  if (t === "Accept" || t === "Reject" || t === "Add") return res.code(200).raw.end();
  if (t === "Remove" || t === "Like" || t === "Announce") return res.code(200).raw.end();
  if (t === "Create" || t === "Update" || t === "Delete") return res.code(200).raw.end();
  if (t === "Follow") {
    if (new URL(y.actor || "about:blank").protocol !== "https:") return res.code(400).send(new Error(res.statusCode));
    const x = await getActivity(username, hostname, y.actor);
    if (!x) return res.code(500).send(new Error(res.statusCode));
    await acceptFollow(username, hostname, x, y);
    return res.code(200).raw.end();
  }
  if (t === "Undo") {
    const z = y.object ?? {};
    t = z.type ?? "";
    if (t === "Accept" || t === "Like" || t === "Announce") return res.code(200).raw.end();
    if (t === "Follow") {
      if (new URL(y.actor || "about:blank").protocol !== "https:") {
        return res.code(400).send(new Error(res.statusCode));
      }
      const x = await getActivity(username, hostname, y.actor);
      if (!x) return res.code(500).send(new Error(res.statusCode));
      await acceptFollow(username, hostname, x, z);
      return res.code(200).raw.end();
    }
  }
  res.code(500).send(new Error(res.statusCode));
});

app.post("/u/:username/outbox", (_req, res) => res.code(405).send(new Error(res.statusCode)));
app.get("/u/:username/outbox", (req, res) => {
  const username = req.params.username;
  const hostname = new URL(CONFIG.origin).hostname;
  if (username !== CONFIG.actor[0].preferredUsername) return res.callNotFound();
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `https://${hostname}/u/${username}/outbox`,
    type: "OrderedCollection",
    totalItems: 0,
  };
  res.type("application/activity+json").send(body);
});

app.get("/u/:username/following", (req, res) => {
  const username = req.params.username;
  const hostname = new URL(CONFIG.origin).hostname;
  if (username !== CONFIG.actor[0].preferredUsername) return res.callNotFound();
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `https://${hostname}/u/${username}/following`,
    type: "OrderedCollection",
    totalItems: 0,
  };
  res.type("application/activity+json").send(body);
});

app.get("/u/:username/followers", (req, res) => {
  const username = req.params.username;
  const hostname = new URL(CONFIG.origin).hostname;
  if (username !== CONFIG.actor[0].preferredUsername) return res.callNotFound();
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `https://${hostname}/u/${username}/followers`,
    type: "OrderedCollection",
    totalItems: 0,
  };
  res.type("application/activity+json").send(body);
});

app.post("/s/:secret/u/:username", async (req, res) => {
  const username = req.params.username;
  const hostname = new URL(CONFIG.origin).hostname;
  const send = req.body;
  const t = send.type ?? "";
  if (username !== CONFIG.actor[0].preferredUsername) return res.callNotFound();
  if (!req.params.secret || req.params.secret === "-") return res.callNotFound();
  if (req.params.secret !== process.env.SECRET) return res.callNotFound();
  if (new URL(send.id || "about:blank").protocol !== "https:") return res.code(400).send(new Error(res.statusCode));
  const x = await getActivity(username, hostname, send.id);
  if (!x) return res.code(500).send(new Error(res.statusCode));
  const aid = x.id ?? "";
  const atype = x.type ?? "";
  if (aid.length > 1024 || atype.length > 64) return c.body(null, 400);
  if (t === "follow") {
    await follow(username, hostname, x);
    return res.code(200).raw.end();
  }
  if (t === "undo_follow") {
    await undoFollow(username, hostname, x);
    return res.code(200).raw.end();
  }
  if (t === "like") {
    if (new URL(x.attributedTo || "about:blank").protocol !== "https:") {
      return res.code(400).send(new Error(res.statusCode));
    }
    const y = await getActivity(username, hostname, x.attributedTo);
    if (!y) return res.code(500).send(new Error(res.statusCode));
    await like(username, hostname, x, y);
    return res.code(200).raw.end();
  }
  if (t === "undo_like") {
    if (new URL(x.attributedTo || "about:blank").protocol !== "https:") {
      return res.code(400).send(new Error(res.statusCode));
    }
    const y = await getActivity(username, hostname, x.attributedTo);
    if (!y) return res.code(500).send(new Error(res.statusCode));
    await undoLike(username, hostname, x, y);
    return res.code(200).raw.end();
  }
  if (t === "announce") {
    if (new URL(x.attributedTo || "about:blank").protocol !== "https:") {
      return res.code(400).send(new Error(res.statusCode));
    }
    const y = await getActivity(username, hostname, x.attributedTo);
    if (!y) return res.code(500).send(new Error(res.statusCode));
    await announce(username, hostname, x, y);
    return res.code(200).raw.end();
  }
  if (t === "undo_announce") {
    if (new URL(x.attributedTo || "about:blank").protocol !== "https:") {
      return res.code(400).send(new Error(res.statusCode));
    }
    const y = await getActivity(username, hostname, x.attributedTo);
    if (!y) return res.code(500).send(new Error(res.statusCode));
    const z = send.url || `https://${hostname}/u/${username}/s/00000000000000000000000000000000`;
    if (new URL(z).protocol !== "https:") return res.code(400).send(new Error(res.statusCode));
    await undoAnnounce(username, hostname, x, y, z);
    return res.code(200).raw.end();
  }
  if (t === "create_note") {
    const y = send.url || "https://localhost";
    if (new URL(y).protocol !== "https:") return res.code(400).send(new Error(res.statusCode));
    await createNote(username, hostname, x, y);
    return res.code(200).raw.end();
  }
  if (t === "create_note_image") {
    const y = send.url || `https://${hostname}/public/logo.png`;
    if (new URL(y).protocol !== "https:") return res.code(400).send(new Error(res.statusCode));
    if (new URL(y).hostname !== hostname) return res.code(400).send(new Error(res.statusCode));
    let z = "image/png";
    if (y.endsWith(".jpg") || y.endsWith(".jpeg")) z = "image/jpeg";
    if (y.endsWith(".svg")) z = "image/svg+xml";
    if (y.endsWith(".gif")) z = "image/gif";
    if (y.endsWith(".webp")) z = "image/webp";
    if (y.endsWith(".avif")) z = "image/avif";
    await createNoteImage(username, hostname, x, y, z);
    return res.code(200).raw.end();
  }
  if (t === "create_note_mention") {
    if (new URL(x.attributedTo || "about:blank").protocol !== "https:") {
      return res.code(400).send(new Error(res.statusCode));
    }
    const y = await getActivity(username, hostname, x.attributedTo);
    if (!y) return res.code(500).send(new Error(res.statusCode));
    const z = send.url || "https://localhost";
    if (new URL(z).protocol !== "https:") return res.code(400).send(new Error(res.statusCode));
    await createNoteMention(username, hostname, x, y, z);
    return res.code(200).raw.end();
  }
  if (t === "create_note_hashtag") {
    const y = send.url || "https://localhost";
    if (new URL(y).protocol !== "https:") return res.code(400).send(new Error(res.statusCode));
    const z = send.tag || "Hashtag";
    await createNoteHashtag(username, hostname, x, y, z);
    return res.code(200).raw.end();
  }
  if (t === "update_note") {
    const y = send.url || `https://${hostname}/u/${username}/s/00000000000000000000000000000000`;
    if (new URL(y).protocol !== "https:") return res.code(400).send(new Error(res.statusCode));
    await updateNote(username, hostname, x, y);
    return res.code(200).raw.end();
  }
  if (t === "delete_tombstone") {
    const y = send.url || `https://${hostname}/u/${username}/s/00000000000000000000000000000000`;
    if (new URL(y).protocol !== "https:") return res.code(400).send(new Error(res.statusCode));
    await deleteTombstone(username, hostname, x, y);
    return res.code(200).raw.end();
  }
  console.log(`TYPE ${aid} ${atype}`);
  res.code(200).raw.end();
});

app.get("/.well-known/nodeinfo", (req, res) => {
  const hostname = new URL(CONFIG.origin).hostname;
  const body = {
    links: [
      {
        rel: "http://nodeinfo.diaspora.software/ns/schema/2.0",
        href: `https://${hostname}/nodeinfo/2.0.json`,
      },
      {
        rel: "http://nodeinfo.diaspora.software/ns/schema/2.1",
        href: `https://${hostname}/nodeinfo/2.1.json`,
      },
    ],
  };
  const headers = {
    "Cache-Control": `public, max-age=${Number(CONFIG.ttl) || 0}, must-revalidate`,
    Vary: "Accept, Accept-Encoding",
  };
  res.headers(headers).send(body);
});

app.get("/.well-known/webfinger", (req, res) => {
  const username = CONFIG.actor[0].preferredUsername;
  const hostname = new URL(CONFIG.origin).hostname;
  const p443 = `https://${hostname}:443/`;
  let resource = req.query.resource;
  let hasResource = false;
  if (resource?.startsWith(p443)) resource = `https://${hostname}/${resource.slice(p443.length)}`;
  if (resource === `acct:${username}@${hostname}`) hasResource = true;
  if (resource === `mailto:${username}@${hostname}`) hasResource = true;
  if (resource === `https://${hostname}/@${username}`) hasResource = true;
  if (resource === `https://${hostname}/u/${username}`) hasResource = true;
  if (resource === `https://${hostname}/user/${username}`) hasResource = true;
  if (resource === `https://${hostname}/users/${username}`) hasResource = true;
  if (!hasResource) return res.callNotFound();
  const body = {
    subject: `acct:${username}@${hostname}`,
    aliases: [
      `mailto:${username}@${hostname}`,
      `https://${hostname}/@${username}`,
      `https://${hostname}/u/${username}`,
      `https://${hostname}/user/${username}`,
      `https://${hostname}/users/${username}`,
    ],
    links: [
      {
        rel: "self",
        type: "application/activity+json",
        href: `https://${hostname}/u/${username}`,
      },
      {
        rel: "http://webfinger.net/rel/avatar",
        type: "image/png",
        href: `https://${hostname}/public/${username}u.png`,
      },
      {
        rel: "http://webfinger.net/rel/profile-page",
        type: "text/plain",
        href: `https://${hostname}/u/${username}`,
      },
    ],
  };
  const headers = {
    "Cache-Control": `public, max-age=${Number(CONFIG.ttl) || 0}, must-revalidate`,
    Vary: "Accept, Accept-Encoding",
  };
  res.headers(headers).type("application/jrd+json").send(body);
});

app.get("/@", (_req, res) => res.redirect("/"));
app.get("/u", (_req, res) => res.redirect("/"));
app.get("/user", (_req, res) => res.redirect("/"));
app.get("/users", (_req, res) => res.redirect("/"));

app.get("/users/:username", (req, res) => res.redirect(`/u/${req.params.username}`));
app.get("/user/:username", (req, res) => res.redirect(`/u/${req.params.username}`));
app.get("/@:username", (req, res) => res.redirect(`/u/${req.params.username}`));

app.listen({ port: process.env.PORT || 8080, host: process.env.HOSTS || "localhost" });
